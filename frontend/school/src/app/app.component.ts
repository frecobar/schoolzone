import { Component } from "@angular/core";

@Component({
  selector: "sz-root",
  template: `<router-outlet></router-outlet>`,
  styles: [],
})
export class AppComponent {
  title = "school";
}
