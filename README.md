# School

# TODO

- Initialize Angular frontend
  - Actual School App
  - School Admin App
  - Product Admin App
  - SandBox app

# Terminal Commands Used

- ng new school --skipInstall=true --minimal=true --createApplication=false --skipGit=true --style=scss --skipTests=true --prefix=sz --packageManager=yarn --newProjectRoot=frontend --directory=./

- ng g application school --minimal=true --routing --style=scss --skipTests=true --prefix=sz
- ng g application admin --minimal=true --routing --style=scss --skipTests=true --prefix=sz
- ng g application sandbox --minimal=true --routing --style=scss --skipTests=true --prefix=sz
- ng g application super --minimal=true --routing --style=scss --skipTests=true --prefix=sz
